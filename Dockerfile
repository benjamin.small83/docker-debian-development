FROM debian:testing
MAINTAINER "Ben Small" <benjamin.small83@gmail.com>

ADD resources/ /resources

# Install packages
RUN apt-get update && \
    apt-get -y --no-install-recommends --no-install-suggests install openssh-server sudo && \
    chmod +x /resources/*.sh && \
    mkdir -p /var/run/sshd && sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config && \
    sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    touch /root/.Xauthority && \
# Set a default user. Available via runtime flag `--user docker`
# Add user to 'staff' group, granting them write privileges to /usr/local/lib/R/site.library
# User should also have & own a home directory, but also be able to sudo
    useradd docker && \
    passwd -d docker && \
    mkdir /home/docker && \
    chown docker:docker /home/docker && \
    addgroup docker staff && \
    addgroup docker sudo && \
# Install dev tools
# Clion likes cmake 3.14.x so we need to pull and build from source to ensure we get the right version
    apt-get -y --no-install-recommends --no-install-suggests install gdb build-essential apt-transport-https ca-certificates gnupg software-properties-common wget rsync && \
    cd /tmp && \
    wget https://github.com/Kitware/CMake/releases/download/v3.14.6/cmake-3.14.6.tar.gz && \
    tar -zxvf cmake-3.14.6.tar.gz && \
    cd cmake-3.14.6 && \
    ./bootstrap && \
    make && \
    make install && \
    rm -rf /tmp/cmake* && \
    apt-get purge && \
    ln -s /usr/local/bin/cmake /usr/bin/cmake

#    apt-get -y --no-install-recommends --no-install-suggests install gdb build-essential apt-transport-https ca-certificates gnupg software-properties-common wget rsync && \
#    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add - && \
#    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
#    apt-get update && \
#    apt-get install kitware-archive-keyring && \
#    apt-key --keyring /etc/apt/trusted.gpg del C1F34CDD40CD72DA && \
#    apt-get install -y cmake

EXPOSE 22
CMD ["/resources/run.sh"]
