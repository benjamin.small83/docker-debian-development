#!/bin/bash

if [ "${SSH_KEY}" ]; then
  for MYHOME in /root /home/docker; do
    echo "=> Adding SSH key to ${MYHOME}"
    mkdir -p ${MYHOME}/.ssh
    chmod go-rwx ${MYHOME}/.ssh
    echo "${SSH_KEY}" > ${MYHOME}/.ssh/authorized_keys
    chmod go-rw ${MYHOME}/.ssh/authorized_keys
    echo "=> Done!"
  done
  chown -R docker:docker /home/docker/.ssh;
elif [ -f /id_rsa.pub ]; then
  echo "=> Using public key file that's been mounted";
  mkdir -p /root/.ssh /home/docker/.ssh;
  cp /id_rsa.pub /root/.ssh/authorized_keys
  cp /id_rsa.pub /home/docker/.ssh/authorized_keys
else
	echo "=> Please pass your public key in the SSH_KEY environment variable"
	exit 1
fi

echo "========================================================================"
echo "You can now connect to this container via SSH using:"
echo ""
echo "    ssh -p <port> <user>@<host>"
echo ""
echo "Choose root (full access) or docker (limited user account) as <user>."
echo "========================================================================"
