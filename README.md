debian-development
==========

Simple Debian/Ubuntu Docker images with *passwordless* SSH access and a regular user
with `sudo` rights


Using
-----

This image is built by [Docker hub](https://registry.hub.docker.com/u/benjaminsmall83/debian-development/).
You can start this image like so:

    docker run --rm -v ~/.ssh/id_rsa.pub:/id_rsa.pub -p50022:22 -d benjaminsmall83/debian-development:latest

This requires a public key in `~/.ssh/id_rsa.pub`.

Two users exist in the container: `root` (superuser) and `docker` (a regular user
with passwordless `sudo`). SSH access using your key will be allowed for both
`root` and `docker` users.
To connect to this container as root:

    ssh -p 50022 root@localhost

To connect to this container as regular user:

    ssh -p 50022 docker@localhost

Change `50022` to any local port number of your choice.
